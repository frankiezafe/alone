tool

extends SpotLight

export (bool) var play:bool = true setget set_play

func set_play(b:bool):
	if initialised != null and play:
		transform = init_transform
	play = b
	if play and initialised:
		init_transform = transform

var initialised:bool = false
var init_transform = null

func _process(delta):
	
	if !initialised:
		initialised = true
		init_transform = transform
	
	if play:
		transform.origin = init_transform.origin + Vector3(rand_range(-.001,.001),rand_range(-.001,.001),rand_range(-.001,.001))
