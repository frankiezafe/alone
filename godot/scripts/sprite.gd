tool

extends MeshInstance

export (bool) var play:bool = true setget set_play
export (int) var frame_offset:int = 0
export (int) var frame_per_line:int = 1 setget set_frame_per_line
export (int) var frame_count:int = 1 setget set_frame_count
export (float,-2,2) var zoom:float = 0 setget set_zoom
export (float,0,30) var speed:float = 1

func set_frame_per_line(i:int):
	frame_per_line = i
	if initialised:
		material_override.set_shader_param( "frame_per_line", frame_per_line )
		$shadow.material_override.set_shader_param( "frame_per_line", frame_per_line )
		
func set_frame_count(i:int):
	frame_count = i
	if initialised:
		material_override.set_shader_param( "frame_count", frame_count )
		$shadow.material_override.set_shader_param( "frame_count", frame_count )

func set_zoom(f:float):
	zoom = f
	if initialised:
		material_override.set_shader_param( "zoom", zoom )
		$shadow.material_override.set_shader_param( "zoom", zoom )

func set_play(b:bool):
	play = b
	if play:
		elapsed_time = frame_offset

var initialised:bool = false
var elapsed_time:float = 0

func _process(delta):
	
	if !initialised:
		initialised = true
		set_frame_per_line(frame_per_line)
		set_frame_count(frame_count)
		set_zoom(zoom)
	
	if !play:
		elapsed_time = 0
	else:
		elapsed_time += delta * speed
	material_override.set_shader_param( "time", elapsed_time )
	$shadow.material_override.set_shader_param( "time", elapsed_time )
