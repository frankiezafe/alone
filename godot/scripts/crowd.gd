extends Spatial

export (int,1,50000) var count:int = 100 setget set_count

func set_count(i:int):
	count = i
	if initialised:
		while $crowd.get_child_count() > 0:
			$crowd.remove_child( get_child(0) )
		var src:MeshInstance = load("res://models/sprite.tscn").instance()
		for i in range( 0, count ):
			var s:MeshInstance = src.duplicate()
			$crowd.add_child( s )
			s.material_override = src.material_override.duplicate()
			s.frame_offset = int(rand_range(0,11))
			s.translation = Vector3( rand_range(-10,10),0,rand_range(-10,10) )
			s.play = true

var initialised:bool = false

func _process(delta):
	if !initialised:
		initialised = true
		set_count( count )
	print( Engine.get_frames_per_second() )
